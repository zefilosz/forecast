package com.zefilosz.mvpforecast.viewmodel

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.ObservableField
import android.view.View
import android.widget.Toast
import com.zefilosz.mvpforecast.CUnit
import com.zefilosz.mvpforecast.model.Forecast

class ItemRepoViewModel (var context: Context, var repository: Forecast): BaseObservable() {

    var dt: ObservableField<String> = ObservableField(repository.dt_txt)
    var temp: ObservableField<String> = ObservableField(
        if(CUnit.isCUnit){
            String.format("%.2f",(repository.main.temp - 273.15))
        }else{
            String.format("%.2f",(((9/5)*(repository.main.temp - 273.15))+ 32))
        }
    )
    var humid: ObservableField<String> = ObservableField(repository.main.humidity.toString())

    companion object {
        fun setRepository(itemRepoViewModel: ItemRepoViewModel, repository: Forecast){
            itemRepoViewModel.repository = repository
            itemRepoViewModel.notifyChange()
        }
    }
}
