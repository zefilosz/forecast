package com.zefilosz.mvpforecast.viewmodel

import android.content.Context
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.zefilosz.mvpforecast.CUnit
import com.zefilosz.mvpforecast.R
import com.zefilosz.mvpforecast.binding.TextWatcherAdapter
import com.zefilosz.mvpforecast.model.ForecastModel
import com.zefilosz.mvpforecast.model.WeatherModel
import com.zefilosz.mvpforecast.repository.ForecastRepository
import com.zefilosz.mvpforecast.repository.WeatherRepository
import io.reactivex.observers.DisposableObserver

class MainViewModel (private var context: Context?, private var repositoryListener: RepositoryListener)
    : BaseViewModel, WeatherRepository.WeatherDataListener, ForecastRepository.ForecastDataListener {

    var infoMessage: ObservableField<String> = ObservableField(context!!.getString(R.string.fill_city_name))
    var isSearch: ObservableBoolean = ObservableBoolean(false)
    var isInfo: ObservableBoolean = ObservableBoolean(true)
    var isProgress: ObservableBoolean = ObservableBoolean(false)
    var isEmpty: ObservableBoolean = ObservableBoolean(true)

    var sUnit: ObservableField<String> = ObservableField("")
    var bUnit: ObservableField<String> = ObservableField("F")

    var wTemp: ObservableField<String> = ObservableField("")
    var wPressure: ObservableField<String> = ObservableField("")
    var wHumidity: ObservableField<String> = ObservableField("")
    var wTempMin: ObservableField<String> = ObservableField("")
    var wTempMax: ObservableField<String> = ObservableField("")

    var wCityName: ObservableField<String> = ObservableField("")
    var wMain: ObservableField<String> = ObservableField("")
    var wDes: ObservableField<String> = ObservableField("")

    private var weatherRespository: WeatherRepository = WeatherRepository(context!!, this)
    private var forecastRespository: ForecastRepository = ForecastRepository(context!!, this)

    var editTextCityNameValue: String = ""

    private var weatherDisposable: DisposableObserver<WeatherModel>? = null
    private var forecastDisposable: DisposableObserver<ForecastModel>? = null

    private var weatherModel: WeatherModel? = null
    private var forecastModel: ForecastModel? = null

    interface RepositoryListener {
        fun  onRepositoriesChanged(repositories: ForecastModel)
    }

    fun onClickSearch(view: View) {
        loadWeatherRepositories(editTextCityNameValue)
        loadForecastRepositories(editTextCityNameValue)
    }

    fun onSwitchUnit(view: View) {
        if(CUnit.isCUnit){
            sUnit.set("F")
            bUnit.set("c")

            CUnit.isCUnit = !CUnit.isCUnit
        }else{
            sUnit.set("C")
            bUnit.set("F")

            CUnit.isCUnit = !CUnit.isCUnit
        }

        if (editTextCityNameValue != ""){
            loadWeatherRepositories(editTextCityNameValue)
            loadForecastRepositories(editTextCityNameValue)
        }
    }

    fun getCityNameEditTextWatcher(): TextWatcher {
        return object : TextWatcherAdapter() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                super.onTextChanged(s, start, before, count)
                editTextCityNameValue = s.toString()
                isSearch.set(s?.length!! > 0)
            }
        }
    }

    fun loadWeatherRepositories(cityName : String) {
        isProgress.set(true)
        isEmpty.set(true)
        isInfo.set(false)
        weatherDisposable  = weatherRespository.loadRepositories(cityName)
    }

    fun loadForecastRepositories(cityName : String) {
        forecastDisposable  = forecastRespository.loadRepositories(cityName)
    }

    override fun onWeatherError(message: String) {
        isProgress.set(false)
        infoMessage.set(message)
        isInfo.set(true)

        wTemp.set("")
        wPressure.set("")
        wHumidity.set("")
        wTempMin.set("")
        wTempMax.set("")

        wCityName.set("")
        wMain.set("")
        wDes.set("")
    }

    override fun onWeatherComplete() {

        wPressure.set(weatherModel!!.main.pressure.toString())
        wHumidity.set(weatherModel!!.main.humidity.toString())

        if (CUnit.isCUnit){
            wTemp.set(String.format("%.2f", (weatherModel!!.main.temp - 273.15)))
            wTempMin.set(String.format("%.2f",(weatherModel!!.main.temp_min - 273.15)))
            wTempMax.set(String.format("%.2f",(weatherModel!!.main.temp_max - 273.15)))
        }else{
            wTemp.set(String.format("%.2f",(((9/5)*((weatherModel!!.main.temp) - 273.15)) + 32 )))
            wTempMin.set(String.format("%.2f",(((9/5)*((weatherModel!!.main.temp_min) - 273.15)) + 32 )))
            wTempMax.set(String.format("%.2f",(((9/5)*((weatherModel!!.main.temp_max) - 273.15)) + 32 )))
        }

        wCityName.set(weatherModel!!.name)
        wMain.set(weatherModel!!.weather[0].main)
        wDes.set(weatherModel!!.weather[0].description)
    }

    override fun onWeatherSuccess(value: WeatherModel?) {
        this.weatherModel = value
    }

    override fun onForecastError(message: String) {
        isProgress.set(false)
        infoMessage.set(message)
        isInfo.set(true)
    }

    override fun onForecastComplete() {

        repositoryListener?.onRepositoriesChanged(forecastModel!!)
        isProgress.set(false)
        if (!forecastModel?.list!!.isEmpty()){
            isEmpty.set(false)
        }else{
            infoMessage.set(context?.getString(R.string.error_city_not_found))
            isInfo.set(true)
        }
    }

    override fun onForecastSuccess(value: ForecastModel?) {
        this.forecastModel = value
        isProgress.set(false)
    }

    override fun destroy() {
        if(!weatherDisposable!!.isDisposed) weatherDisposable!!.dispose()
        if(!forecastDisposable!!.isDisposed) forecastDisposable!!.dispose()
        weatherDisposable = null
        forecastDisposable = null
        context = null
    }
}