package com.zefilosz.mvpforecast.api

import com.zefilosz.mvpforecast.API_FORECAST
import com.zefilosz.mvpforecast.API_WEATHER
import com.zefilosz.mvpforecast.model.ForecastModel
import com.zefilosz.mvpforecast.model.WeatherModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET (API_WEATHER)
    fun getWeather(@Query("q") q: String,
                   @Query("appid") appid: String): Observable<WeatherModel>

    @GET (API_FORECAST)
    fun getForecast(@Query("q") q: String,
                   @Query("appid") appid: String): Observable<ForecastModel>
}