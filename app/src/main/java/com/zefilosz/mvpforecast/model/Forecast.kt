package com.zefilosz.mvpforecast.model

data class Forecast (
        val weather: List<Weather>,
        val dt_txt: String,
        val clouds: Cloud,
        val wind: Wind,
        val main: WeatherMain
)