package com.zefilosz.mvpforecast.model

data class WeatherModel (
        val id: String,
        val name: String,
        val clouds: Cloud,
        val wind: Wind,
        val weather: List<Weather>,
        val main: WeatherMain
)





