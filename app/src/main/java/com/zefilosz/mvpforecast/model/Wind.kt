package com.zefilosz.mvpforecast.model

data class Wind (
        val speed: Double
)