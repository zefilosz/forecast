package com.zefilosz.mvpforecast.model

data class ForecastModel (
    val cod: String,
    val cnt: Int,
    val list: List<Forecast>
)