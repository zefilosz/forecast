package com.zefilosz.mvpforecast.model

data class WeatherMain (
        var temp : Double,
        val pressure : Double,
        val humidity : Int,
        val temp_min : Double,
        val temp_max : Double
)