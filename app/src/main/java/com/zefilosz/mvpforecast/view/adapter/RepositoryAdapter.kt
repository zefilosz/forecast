package com.zefilosz.mvpforecast.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.zefilosz.mvpforecast.R
import com.zefilosz.mvpforecast.model.Forecast
import com.zefilosz.mvpforecast.databinding.ItemRepoBinding
import com.zefilosz.mvpforecast.viewmodel.ItemRepoViewModel
import java.util.*

class RepositoryAdapter : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.bindRepository(repositories[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var binding = DataBindingUtil.inflate<ItemRepoBinding>(LayoutInflater.from(parent?.context),
                R.layout.item_repo, parent, false)
        return ViewHolder(binding)
    }

    private var repositories: List<Forecast> = Collections.emptyList()

    fun setRepositories(repositories: List<Forecast>) {
        this.repositories   = repositories
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    class ViewHolder(var binding: ItemRepoBinding) : RecyclerView.ViewHolder(binding.layoutContent) {

        fun bindRepository(repository: Forecast) {
            if (binding.vm == null){
                binding.vm = ItemRepoViewModel(itemView.context, repository)
            } else {
                ItemRepoViewModel.setRepository(binding.vm!!, repository)
            }
        }
    }
}