package com.zefilosz.mvpforecast.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.zefilosz.mvpforecast.R
import com.zefilosz.mvpforecast.viewmodel.MainViewModel
import com.zefilosz.mvpforecast.databinding.ActivityMainBinding
import com.zefilosz.mvpforecast.helper.Util
import com.zefilosz.mvpforecast.model.ForecastModel
import com.zefilosz.mvpforecast.model.WeatherModel
import com.zefilosz.mvpforecast.view.adapter.RepositoryAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainViewModel.RepositoryListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    var adapter: RepositoryAdapter? = null

    private fun activityView() {
        binding.editTextUsername?.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val username = binding.editTextUsername.text
                    if (username.isNotEmpty())
                        viewModel.loadWeatherRepositories(username.toString())
                        viewModel.loadForecastRepositories(username.toString())
                    return true
                }
                return false
            }
        })

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupBinding()
        setupRecycleView()
        //setSupportActionBar(toolbar)
        activityView()
    }

    private fun setupRecycleView() {
        adapter = RepositoryAdapter()
        binding.reposRecyclerView.adapter = adapter
        binding.reposRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    override fun onRepositoriesChanged(repositories: ForecastModel) {
        adapter?.setRepositories(repositories.list)
        adapter?.notifyDataSetChanged()
        Util.hideSoftKeyboard(this, binding.editTextUsername)
    }

    private fun setupBinding() {
        viewModel   = MainViewModel(this, this)
        binding.vm  = viewModel
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.destroy()
    }
}
