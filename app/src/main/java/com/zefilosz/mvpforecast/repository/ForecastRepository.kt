package com.zefilosz.mvpforecast.repository

import android.content.Context
import com.zefilosz.mvpforecast.API_ID
import com.zefilosz.mvpforecast.App
import com.zefilosz.mvpforecast.R
import com.zefilosz.mvpforecast.api.ApiService
import com.zefilosz.mvpforecast.api.ServiceFactory
import com.zefilosz.mvpforecast.model.ForecastModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

class ForecastRepository (var context: Context, var dataListener: ForecastDataListener) {

    interface ForecastDataListener {
        fun onForecastError(message: String)
        fun onForecastComplete()
        fun onForecastSuccess(value: ForecastModel?)
    }

    private var factory: ServiceFactory = ServiceFactory()
    private var api: ApiService = factory.provideApi()

    fun loadRepositories(cityName : String): DisposableObserver<ForecastModel>? {
        return api.getForecast(cityName, API_ID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(ForecastObserver())
    }

    private inner class ForecastObserver: DisposableObserver<ForecastModel>() {
        override fun onComplete() {
            dataListener.onForecastComplete()
        }

        override fun onNext(value: ForecastModel?) {
            dataListener.onForecastSuccess(value)
        }

        override fun onError(e: Throwable?) {
            dataListener.onForecastError(context?.getString(R.string.error_city_not_found))
        }
    }
}