package com.zefilosz.mvpforecast.repository

import android.content.Context
import com.zefilosz.mvpforecast.API_ID
import com.zefilosz.mvpforecast.App
import com.zefilosz.mvpforecast.R
import com.zefilosz.mvpforecast.api.ApiService
import com.zefilosz.mvpforecast.api.ServiceFactory
import com.zefilosz.mvpforecast.model.WeatherModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

class WeatherRepository (var context: Context, var dataListener: WeatherDataListener) {

    interface WeatherDataListener {
        fun onWeatherError(message: String)
        fun onWeatherComplete()
        fun onWeatherSuccess(value: WeatherModel?)
    }

    private var factory: ServiceFactory = ServiceFactory()
    private var api: ApiService = factory.provideApi()

    /**
     * load repositories
     */
    fun loadRepositories(cityName : String): DisposableObserver<WeatherModel>? {
        return api.getWeather(cityName, API_ID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(WeatherObserver())
    }

    private inner class WeatherObserver: DisposableObserver<WeatherModel>() {

        override fun onComplete() {
            dataListener.onWeatherComplete()
        }

        override fun onError(e: Throwable?) {
            dataListener.onWeatherError(context?.getString(R.string.error_city_not_found))
        }

        override fun onNext(value: WeatherModel?) {
            dataListener.onWeatherSuccess(value)
        }
    }
}